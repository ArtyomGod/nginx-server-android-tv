# Nginx-server-android-tv

1) Создай папку для фильмов и для nginx.conf (default.conf)
    Например: /my/folder     папка в которой будут лежать ваши файлы и папки
    /my/default.conf    путь до 'default.conf'
2) Скопируй из репозитория в папку /my/  файл  'default.conf'

3)проверь установлен ли у тебя docker-compose 
    если да, то используй команду находясь в папке репозитория
        $ docker-compose -f "docker-compose.yml" up -d --build

4) Чтобы остановить испольщуй команду
    $ docker container stop my-nginx-server
